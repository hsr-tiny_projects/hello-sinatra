# Demo Sinatra with Kubernetes

#### Prerequisites

Ensure Docker, kubectl, and MiniKube are all installed locally and running.

#### Prep the app and install it in MiniKube

```bash
# First, clone this app and enter the directory
git clone https://gitlab.com/hsr-tiny_projects/hello-sinatra.git
cd hello-sinatra

# Ensure we're going to use MiniKube as the Registry for this project
eval $(minikube docker-env)

# Build the image
docker build -t hello-sinatra:v1 .

# Create a Deployment
kubectl run hello-sinatra --image=hello-sinatra:v1 --port=4567

# Create a Service
kubectl expose deployment hello-sinatra --type=LoadBalancer

# Go to the app
minikube service hello-sinatra
```

#### Update app

```bash
kubectl set image deployment/hello-sinatra hello-sinatra=hello-sinatra:v2
```

#### Cleanup

```bash
# Remove the Service and Deployment
kubectl delete service hello-sinatra
kubectl delete deployment hello-sinatra

# Remove MiniKube as the Registry
eval $(minikube docker-env -u)
```

Sources used:
- https://kubernetes.io/docs/tutorials/stateless-application/hello-minikube/
- https://rubyplus.com/articles/2461-Docker-Basics-Running-a-Hello-World-Sinatra-App-in-a-Container
